#!/bin/bash

set -ex

virtualenv .venv

. .venv/bin/activate

pip install -r app/requirements.txt

coverage run app/test_app.py

coverage report app/app.py

