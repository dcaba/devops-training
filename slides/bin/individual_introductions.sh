#!/bin/bash

# default global_opts
exec_root_dir=$(dirname $0)/..
local_introductions_dir=src/0_welcome_presentaciones
host=devops-training.duckdns.org
#host=192.168.102.225
port=44044
#port=22
user=devops-slides-ci
remote_introductions_dir=presentaciones

function download_src_introductions {
	local_folder=$1
	cd $local_folder
	sftp -P $port $user@$host -b <<-EOF                                 
	cd $remote_introductions_dir
	get *.md
	EOF
	cd -
}	

# main()

cd $exec_root_dir

mkdir -p $local_introductions_dir
download_src_introductions $local_introductions_dir
dos2unix $local_introductions_dir/*.md
for file in $(ls $local_introductions_dir/*.md); do
	if [[ "$(file $file | grep ISO-8859 | wc -l)" != "0" ]]; then 
		iconv --from-code=ISO-8859-1 --to-code=UTF-8 $file > $file.new
		mv $file.new $file
	fi
	echo "" >> $file
done

bin/build-docker.sh $local_introductions_dir

