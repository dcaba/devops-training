#!/usr/bin/env bash

set -euo pipefail
exec_root_dir=$(dirname $0)/..
cd $exec_root_dir

# main()
container_name="devops-slides-srv"
docker stop $container_name
docker rm $container_name
