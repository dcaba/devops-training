#!/usr/bin/env bash

set -euo pipefail
exec_root_dir=$(dirname $0)/..
cd $exec_root_dir

# main()
cmdline="docker run -d -p 80:8080 --name devops-slides-srv devops-slides:latest"
echo "INFO: Running \"$cmdline\" under \"$(pwd)\""
eval $cmdline

google-chrome localhost
