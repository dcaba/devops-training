#!/usr/bin/env bash

set -euo pipefail
exec_root_dir=$(dirname $0)/..
cd $exec_root_dir

# main()
cmdline="docker build -t devops-slides:latest ."
echo "INFO: Running \"$cmdline\" under \"$(pwd)\""
eval $cmdline
