#

## Tercera Parte: Modificaciones extras

## Modifiquemos nuestra IaC

* Cambiar el puerto del ELB del puerto **80** al puerto **6789**
* Incrementar el número de servidores a 4
* Cambiar la aplicación para que acepte el path **"/devops"** y retorne el string **"27-09-2019"**
* Añadir en la respuesta de la aplicación la IP privada de la máquina que responde

![](keep-calm-and-do-it-yourself-13.png){ width=30% }

## ¡Redespleguemos!

Para cada cambio, recuerda reflejar los cambios en código, actualizar tu stack y ver como los cambios se despliegan automáticamente

![](https://davidjellison.files.wordpress.com/2013/10/deploy-button.jpg){ width=30% }

Si no hay cambios en la infraestructura (por ejemplo, cambios en el container), CF no actualizará el Stack, en ese caso podemos simular el rollout mediante la terminación de instancias del Autoscaling Group.

## ¡Destruyamos la infraestructura!

Destruye tu stack de Cloudformation, probablemente no lo quieres activo toda la semana: *Delete Stack*

![](http://becomeatopweddingplannerblog.com/wp-content/uploads/2015/01/011215WeddingPlannersWasteMoneyV2.jpg){ width=20% }
