#

## Historia desde el punto de vista de Desarrollo (SWE)

![](https://pbs.twimg.com/media/B_ZW06mUIAA1IX0.png){ width=150% }

## Preámbulo

**5** lecciones que los ingenieros de software aprendieron, ayudando a trazar un camino distinto

![](http://jlcollinsnh.com/wp-content/uploads/2013/06/new-path.jpg)

## 1. La importancia de los tests

Los ingenieros de software introducen el testing continuo como uno de los pilares para el desarrollo de software.

Ventaja principal:

> **Confianza** ante refactors, upgrades o cambios en componentes fuera de su control

##

Cubrirlos de una forma pragmática facilita su automatización, identificándose:

* Unit
* Integration
* Regression
* Functional
* Performance
* Acceptance

## 2. No acertarás a la primera

* Por mucho que medites una decisión, **puedes fallar**.
* El negocio también evolucionará y cambiará de dirección, con los requisitos, use cases y herramientas que proporcionas.
* Otras condiciones (tecnología, proveedores...) también cambiarán, dejando tus decisiones como obsoletas.

##

Por lo tanto...

* **Mide** la efectividad de las soluciones

* **Mejora** tus soluciones; asume que nada es un "one-time"

* El mejor input para definir los siguientes pasos es el feedback de los usuarios

    * **Entrega algo pronto**, que puedas rectificar
    * ... **mejor que algo perfecto tarde**, que no podrás cambiar

* El **feedback inmediato** es básico

    * A más tiempo sucede entre la implementación de una solución y "volver a ella", más ineficiente es el proceso

## 3. Todo es cíclico

![](https://upload.wikimedia.org/wikipedia/commons/5/5f/Three_software_development_patterns_mashed_together.svg){width=65%}

## 4. La mejora continua como un pilar

* No eres el primero en enfrentarte a un problema. Y la gestión del trabajo es un problema ampliamente estudiado.

    * Los equipos con más visión ven el valor de la **mejora continua**, portándola de **Lean**.

* Identificar bloqueantes (Sistemas suele ser uno de ellos, que además no crece de forma proporcional a los equipos de desarrollo) y otros elementos que afectan a la velocidad y entrega de código, empujan la introducción prácticas que impulsan la autonomía, el alineamiento con el negocio y la creación de equipos sólidos: "**Agile Software Development**".

    * *Los equipos de Sistemas, Operaciones, DBAs, Infraestructura... suelen quedar fuera*

* Como suma y consecuencia de ésto, **el QA y procesos asociados a los despliegues evolucionan mucho estos últimos 5-10 años.**

## 5. Valor del trabajo en equipo

* Equipos compartiendo la toma de decisiones, y **asumiendo de forma colectiva todo el "producto", multiplican la efectividad** de los esfuerzos individuales
* La **compartición de conocimiento y prácticas aplicadas** pasa a ser un valor de la comunidad de desarrollo de software, ganando "adopters" en todo el mundo y haciendo que la rueda gire aún a más velocidad
* Filosofía **"You build it, you run it"**

#

## Ahora sí, historia de las prácticas que se introducen...

## Integración continua

* **Cada commit debe llevar a la aplicación a un estado funcional**
* Automatiza la construcción de tu aplicación
* Feedback inmediato del resultado de la ejecución de los tests, desde un entorno más similar al productivo

##

Herramientas para construir tu aplicación:

* make > Ant > Maven > Gradle > Bazel

Herramientas para disparar y "reportar" estado:

* CruiseControl > Jenkins (Hudson?) > TeamCity, Travis...

![](https://dzone.com/storage/temp/1388344-top-8-continuous-integration-tools.png)

## Trabajo conjunto / Team-building

* Pair programming
* Gitflow (PRs, peer reviews)

## Test Driven Development (TDD)

* Tests como mecanismo para representar requisitos y asegurarse que la implementación es operativa desde la primera línea

## Toma control de tus dependencias

Gestiona / representa como código / versiona:

* Paquetes de los que dependas
* Infraestructura que necesites
* Tu esquema de base de datos
* Configuración de tus componentes
* Tus tests
* La construcción de tu aplicación y despliegue

##

Como consecuencia:

* Las aplicaciones pasan a empaquetar / embeber todo lo que necesitan para ejecutarse
* Los containers **NO** son una VM ligera
    * **Es una forma de entregar software y todo lo que éste necesita para funcionar**

## Entrega continua

* Existen diferentes modelos y prácticas para actualizar la aplicación (y todo el stack!)
* Introducción de la infraestructura inmutable
* Feature switches
    * El release de código se desacopla de la activación de funcionalidades

> **No es raro tener equipos de desarrollo desplegando (y testeando) decenas o centenares de veces al día**


## Arquitecturas y equipos escalables

* IaaS/PaaS te puede permitir olvidarte de la gestión de capacidad y minimizar tus interdependencias a "APIs"
* El uso de microservicios (llevando más allá alguno de los pilares establecidos por SOA) también permite que tus equipos (y responsabilidades asociadas) escalen minimizando dependencias entre éstos
    * Y mejor si son transversales: **"You build it, you run it"** (otra vez)
