# 

## Historia desde el punto de vista de Operaciones

![](https://pbs.twimg.com/media/CLQAt-nUkAAWa7m.jpg){ width=140% }


## Hosts multi-usuarios / multi-uso

* Legado de entornos "host"/mainframe/UNIX
* Retos como compilar código, buscar/construir paquetes de SO, etc. 
* Gestión de configuraciones arcaica (Afectación a usuarios? Reinicios?)
    * Administradores "avanzados" replicaban configuraciones via scripting...
    * ... u optaban por servicios que centralizaban configuraciones.

![](https://www.howtogeek.com/wp-content/uploads/2014/02/xken-thompson-and-dennis-richie-at-pdp-11.jpg.pagespeed.gp+jp+jw+pj+ws+js+rj+rp+rw+ri+cp+md.ic.2QmT2PL9xY.jpg)

## Aplicaciones distribuidas

<div id="right">
* Inversión en destinar máquinas a propósitos específicos
    * Facilitando la identificación de problemas
    * La gestión de capacidad
    * O la securización de los backends
* A su vez, las aplicaciones empiezan a romperse por capas
    * Web
    * Aplicación
    * Base de datos
</div>
<div id="left">
![](https://docs.microsoft.com/en-us/vsts/tfs-server/_img/complex-multi-domain-topo.png)
</div>

## Alta disponibilidad

<div id="left">
* Clusters "tradicionales" 
    * "discos compartidos"
</div>

<div id="right">
* Shared nothing: 
    * Nodos balanceados 
    * Data sharding
</div>

![](http://benstopford.com/uploads/snsd2.jpg)

## Llegada de la virtualización

<div id="left">
Inicialmente como un mero mapeo de:

> una máquina tradicional <-> una máquina virtual

Con la ventaja de ahorrar "hierro" y hacer mas ligera la provisión de entornos
</div>

<div id="right">
![](https://pbs.twimg.com/media/ByFZm8iIcAA4wJs.jpg)
</div>

## 

![](vmware1.png){width=75%}

## Lenta adopción de la virtualización

* Durante largos años no se potencian **otras ventajas** (capacidad dinámica, replicación y estandarización de entornos...), sumado a un dominio de sistemas propietarios, con lo que el overhead limita su impacto en entornos de producción
* Es gracioso (a la vez de triste) que sólo la ampliación de las matrices de compatibilidad (para evitar que sistemas no soportados entraran en conflicto con renovaciones hardware), y el hecho de que el clustering de hipervisores mitigue la falta de HA en ciertas aplicaciones, ha llevado a la virtualización a reemplazar a algunos entornos físicos
    * ESXi6 (estará en soporte hasta 2020) soporta Windows 2000 (ahora con 17 años, y casi 7 en End of Life)
    * ESXi5.1 soportaba hasta Windows NT4 (1996)

## Hipervisores

* VMWare 
    * ESX
    * ESXi
* Microsoft:
    * Hyper-v
* Diferentes alternativas GNU
    * Xen
    * KVM
    * Virtualbox

## "Orquestradores" de VMs

* vSphere suite 
* SCVMM / Hyper-V manager
* Openstack

## ¿Virtualización como poder de cambio?

> *"All problems in computer science can be solved by another level of indirection/abstraction"*, David Wheeler

* De nuevo, los problemas en informática se solucionan añadiendo una capa de abstracción
* Las máquinas virtuales pasan a estar más cerca del software que de los servidores
* Clonación y creación de templates como herramienta para replicar entornos
    * Con ello, la generación de entornos distribuidos se simplifica
* Snapshots como mecanismo para quick recovery & rollback
* Incluso se llegan a implementar integraciones entre el release de aplicaciones y operaciones sobre la infraestructura virtual

## Virtualización en la nube

* Proveedores de Cloud como hosting externo de máquinas virtuales?
    * Vsphere Air?
    * Azure (inicialmente)?
* Aplicar dicho mindset en AWS generó fricciones desde los inicios
    * EC2 no aportaba discos persistentes por defecto
    * AWS se diferencia desde el inicio por el gran catálogo de servicios consumibles via API

## Explosión del número de sistemas

La virtualización y proveedores Cloud hacen que el número de sistemas se multiplique:

* Antiguamente el número de sistemas dependía del número de máquinas, y por ende, estaba asociado al budget
* Esta relación se rompe, especialmente con el overcommitment y el pago por uso

![](http://royal.pingdom.com/wp-content/uploads/2012/07/netcraft.png)

## Gestión de configuraciones centralizado

<div id="left">
La evolución de sistemas de centralización de configuraciones (y orquestración de cambios) pasa a ser imprescindible:

* cfEngine, Puppet, Fabric, Chef, Salt, Ansible, etc.

</div>

<div id="right">
![](https://www.getfilecloud.com/blog/wp-content/uploads/2014/07/CMTs1.jpg){width=70%}
</div>

¡Los "sysadmins" más intrépidos incluso intentan conectar estos frameworks con el release de aplicaciones!

* ¿Jenkins como disparador?
* ¿Preparación de entornos via vagrant?

## Intentando cubrir PaaS

Iniciativas como:

* vApps (Conjunto de VMs empaquetadas y distribuidas como una unidad (modo "caja negra") para distribuir plataformas / aplicaciones complejas),
* Herramientas self-service (Catálogos de servicios IT personalizables y automáticamente desplegables)
* ó Cloud Foundry (plataforma desplegable en IaaS para dar funcionalidades PaaS) 

Intentan **acercar la infraestructura al resto de la organización**, dejando despliegues antes complejos a "golpes de click".

##

![](http://dreamand.me/img/posts/cloud/cloudfoundry-marketplace.png)

## Popularización de los contenedores

> Contenedores como máquinas virtuales ligeras?

![](http://cdn.ttgtmedia.com/rms/onlineImages/windows_server-virtual_machines_vs_containers_desktop.png){width=50%}

* LXC (namespaces, cgroups)
* Docker (y rkt)

## ¿Repitiendo el camino de la virtualización?

![](VirtContHA.png)
[*Original paper*](https://pdfs.semanticscholar.org/c472/f06def25f702dabf77c74c476729d9e4c393.pdf)


## Orquestración de contenedores

Proporcionan un lenguaje para describir una plataforma:

* potencialmente compuesta por múltiples "servicios"
* y componentes comúnmente necesarios
* más la infraestructura necesaria (load balancers, bases de datos, etc.)

y mecanismos para actualizarla.

<div id=left>
Frameworks más extendidos: 

* Docker Swarm
* Apache Mesos
* (Google) Kubernetes

</div>

<div id=right>
![](https://www.altoros.com/blog/wp-content/uploads/2016/03/choose-container-orchestration-engine-apache-mesos-kubernetes-docker-swarm.jpg)
</div>

