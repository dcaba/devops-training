#

## Algunos gráficos que dan para pensar

![](https://cdn.pixabay.com/photo/2015/11/20/06/27/isolated-1052504_960_720.png){ width=60% }

#

## Servidores

##

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Internet_Hosts_Count_log.svg/800px-Internet_Hosts_Count_log.svg.png)

##

![](wpid-site_count_history-png.gif){ width=140% }

##

![](https://www.infragistics.com/community/cfs-filesystemfile.ashx/__key/CommunityServer.Blogs.Components.WeblogFiles/mobileman/3463.data_2D00_growth_2D00_big_2D00_data.png){ width=120% }

#

## Y ahora los que más me asustan de verdad... clientes

##

![](https://whatsthebigdata.files.wordpress.com/2012/08/pc_phone_tablet_1975-20111.png)

##

![](https://www.cisco.com/c/dam/en_us/about/security/images/csc_child_pages/white_papers/iot-figure1.jpg)

##

![](GOTW_3_30_14.png)

#

## ¿Actuar como "Google" es hype o necesidad?

![](http://static2.businessinsider.com/image/57226f7152bcd066018bfb6f-1190-625/5-reasons-google-is-the-best-place-to-work-in-america-and-no-other-company-can-touch-it.jpg)
