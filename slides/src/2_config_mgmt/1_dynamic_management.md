#

## Gestión de la configuración de servicios e Infraestructura

## Objetivos

* Cambiar el **estado** de servidores y/o aplicaciones
* Evitar **errores** por provisión manual
* Evitar el **drift** de configuración entre servidores, mantener **consistencia**
* Facilitar compartir el **ownership** de los cambios entre todo el equipo
* **Reusar buenas prácticas** en toda la flota de servidores
* Permitir la **reproducción de múltiples entornos** de trabajo

## Requerimientos

1. Un nuevo servidor ha de poder ser **provisionado bajo demanda**, en pocos minutos o segundos
2. El proceso de provisión y aplicación de cambios debe ser **repetible, consistente, auto-documentado y transparente**
    * Idealmente *idempotente*
3. Un cambio de provisión o configuración se debe aplicar a los servidores **sin interacción humana**
4. Cada cambio se **aplica a todos** los servidores afectados, y también en los nuevos servidores creados
5. **Tests automáticos** deben ejecutarse para cualquier nuevo cambio
6. Debe ser sencillo y seguro hacer cambios al propio proceso
7. Los cambios de configuración o de proceso se deben **versionar y aplicar a diferentes entornos**
    * ¿PR como oportunidad para discusiones y validaciones del resto del equipo?

## Ciclo de vida de un servidor

![](https://docs.google.com/drawings/d/1l5k3oznLENWk18a982_TLx-c8zV1RrLIkZC1iRuKXOs/pub?w=961&h=329)

## Modelos de gestión cambios en servidores

* Gestión de cambios ad hoc
* Actualización continua de cambios
* Infraestructura Inmutable
* Contenedores

## Gestión de cambios ad hoc

* Tradicionalmente la configuración en cada servidor sólo se tocaba hasta que ser requería un cambio concreto
* Incluso el uso de herramientas de gestión de configuración sólo se aplicaban en determinados momentos
* Esto lleva a **inconsistencia de configuraciones**, así como la **falta de conocimiento** real del **nivel de personalización** de las configuraciones
    * Dificulta la automatización

## Actualización continua de cambios

* La mayoría de herramientas de configuración han sido diseñadas para aplicar un modelo de sincronización continua
* Esta práctica permite mantener la disciplina necesaria en un entorno automatizado
* **Problema**: no toda la configuración del servidor está definida por estas herramientas, siempre pueden quedar cosas fuera de control

## Infraestructura Inmutable

* Cada cambio en la configuración de un servidor implica **volver a recrear un nuevo servidor**, en lugar de aplicar los cambios al servidor anterior
* Toda la configuración del servidor debe estar definida en la plantilla y por tanto siempre es predecible
* La plantilla contiene la imagen origen y un conjunto de scripts y/o definiciones de configuración en tiempo de arranque
* Evita drift (diferencias entre servidores que deberían ser idénticos)
* ¿Dificultades?
    * Para pequeños cambios parece excesivo => Automatizar
    * Ciertas infraestructuras pueden mantener un estado fuera de su configuración (pe., nivel de escalado, logs)
    * Debugar es más lento y complicado => drift y autodestruir o preparar la imagen de referencia (golden image)
    * El tiempo del despliegue se puede incrementar
    * Guardar datos stateful => externalizar (BBDD o ficheros) o trabajar con contenedores

<!-- https://www.slideshare.net/jpetazzo/immutable-infrastructure-with-docker-and-containers-gluecon-2015 -->
<!-- https://www.nginx.com/blog/devops-and-immutable-delivery/ -->

## Contenedores

* El uso de contenedores nos permite ir un paso más allá, pues los servidores host sólo sirven para correr contenedores
* Por tanto, su plantilla y configuración es minimalista
* El nivel de abstracción con las aplicaciones permite desacoplar e introducir cambios sin implicaciones en éstas
* La mayoría de cambios en el servidor son relativos a las aplicaciones y por tanto sólo implican a los contenedores
* Es el concepto de **infraestrucutra inmutable aplicada a nivel de aplicación**
