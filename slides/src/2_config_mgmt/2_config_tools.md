#

##  Herramientas para IaC


## Imperativo vs Declarativo

![](imperative-vs-declarative-xkcd.png){ width=120% }

<!-- ## Arquitectura Agent vs Agent-less -->

<!-- https://www.slideshare.net/MartinEtmajer/automated-deployments-with-ansible -->


## Herramientas para gestión de configuración en servidores
 
| Herramienta | Método | Aproximación |
| ------ | ------- | ------- |
| CFEngine | Pull | Declarativo |
| Chef | Pull | Imperativo |
| Puppet | Pull | Declarativo |
| SaltStack | Push | Declarativo e Imperativo |
| Ansible | Push | Declarativo e Imperativo |

## Popularidad

![](googletrends.png){ width=80% }
