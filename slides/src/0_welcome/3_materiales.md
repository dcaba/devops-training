#

## Y los materiales?

Podréis encontrar tanto la presentación como las prácticas en el siguiente repositorio:

[https://bitbucket.org/dcaba/devops-training](https://bitbucket.org/dcaba/devops-training)

> *BTW: La presentación está "definida como código" :)*

## ¡Vuestras aportaciones son bienvenidas!

* Podrás compartir feedback al final del curso
* Grupo para comentarios / preguntas tras el curso: [devops-training-bcn@googlegroups.com](mailto:devops-training-bcn@googlegroups.com)
* Aceptamos PRs :)

#

## ¿Qué necesitáis para afrontar el curso?

* Mente abierta y despierta
* Ganas de "ensuciaros las manos"
* Paciencia

![](http://www.elcomcms.com/Images/UserUploadedImages/870/company-morale.jpg)

## Nosotros os ayudaremos a...

* Digerir **nuevos conceptos** mediante **ejemplos prácticos**
* Enfrentaros a la parte práctica, aunque *"no estéis al día"*

![](http://manukleart.com/wp-content/uploads/2015/02/manukleart-coaching-helping.jpg){ width=50% }

## Ready?

![](https://68.media.tumblr.com/tumblr_lo0vprgRdW1qa54hvo1_500.gif)
