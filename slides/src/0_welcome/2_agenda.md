#

## Agenda

## 22 de Noviembre (Viernes)

| Hora | Sección |
| --- | --------- |
| 17:00-17:30 | 0 - Welcome y apertura |
| 17:30-18:30 | 1 - DevOps como alternativa a la gestión de infraestructuras |
| 18:30-18:45 | *Break* |
| 18:45-19:45 | 2 - Gestión de Infraestructura Dinámica |
| 19:45-21:00 | Práctica A: Administración de infrastructura con Ansible |

## 23 de Noviembre (Sábado)

| Hora | Sección |
| --- | --------- |
| 9:00-10:30 | 3 - Ingeniería de Software para la gestión de infraestructuras |
| 10:30-10:45 | *Break* |
| 10:45-12:15 | 4 - Aparición del Cloud |
| 12:15-12:30 | *Break* |
| 12:30-14:00 | Práctica B1: Del código al Cloud Provider |

## 29 de Noviembre (Viernes)

| Hora | Sección |
| ----- | --------- |
| 17:00:17:30 | Recap |
| 17:30-18:30 | 5 - Entregando software |
| 18:30-18:45 | *Break* |
| 18:45-21:00 | Práctica B2: CI/CD de infraestructura dinámica |

## 30 de Noviembre (Sábado)

| Hora | Sección |
| ----- | --------- |
| 9:00-10:45 | 6 - Nuevas tendencias en arquitecturas IT |
| 10:45-11:00 | *Break* |
| 11:00-12:15 | Práctica C: Despliegue en cluster Kubernetes |
| 12:15-12:30 | *Break* |
| 12:30-13:15 | 7 - Ideas para favorecer la adopción de la cultura DevOps |
| 13:15-14:00 | Wrap-up y cierre |
