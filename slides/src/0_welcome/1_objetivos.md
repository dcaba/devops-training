#

## Objetivos del curso

* Presentar cómo la **cultura** DevOps permite enfrentarnos a los retos actuales de IT
* Mostrar de forma **progresiva y práctica** las herramientas que nos ayudarán a afrontar dichos retos
* Ayudaros a aproximar nuevos problemas con un **punto de vista distinto** 
* Dar unos **primeros pasos** en la dirección que esperamos queráis recorrer en los próximos años

