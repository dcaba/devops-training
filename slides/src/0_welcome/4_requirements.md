#

## ¿Qué podemos ir preparando?

* Cuenta en Bitbucket
    * https://bitbucket.org
* Cuenta en AWS
    * https://portal.aws.amazon.com/gp/aws/developer/registration/index.html
    * Validar que es posible crear recursos en EC2, como una "key pair" (podría requerir validación telefónica)
* Cuenta en DockerHub
    * https://hub.docker.com/
* Descargar e instalar VirtualBox
    * https://www.virtualbox.org/wiki/Downloads
* Descargar e instalar Vagrant
    * https://www.vagrantup.com/downloads.html
* Descargar e instalar Minikube
    * https://kubernetes.io/docs/tasks/tools/install-minikube/
* Descargar e instalar Ansible
    * https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
* Descargar e instalar cliente GIT y SFTP
* Descargar imagen de la VM que usaremos en las prácticas (*ver siguiente slide*)

## Inicializamos máquinas virtuales

Para facilitar el desarrollo del curso independientemente del sistema operativo en vuestra máquina, utilizaremos una máquina virtual gestionada por Vagrant, y otra por minikube. Para precargar las
imágenes de las máquinas virtuales que usaremos en las prácticas:
```bash
vagrant init williamyeh/ubuntu-trusty64-docker
vagrant up; vagrant halt
minikube start; minikube stop
```

En caso de problemas, ver la siguiente slide

## Errores conocidos

> En caso de problemas, y de partir de una instalación vieja de VirtualBox/Vagrant, te recomendamos limpiar el sistema e instalar las ultimas versiones

> En caso de fallo inicializando Vagrant, asegúrate que el soporte de VT-x se encuentra habilitado en la BIOS.

> En entornos MS Windows, asegúrate que Windows Hyper-V está desactivado en "Windows Features on or off"
