#

## Gestión dinámica de la capacidad

¡Uno de los grandes beneficios del Cloud!

¿En base a qué decidimos aumentar/disminuir nuestra capacidad?

* Eventos preprogramados (pre-warming; efectos estacionales)
* Utilización de CPU
* Métricas de sistema
* Número de trabajos/mensajes pendientes

<div id="left">
![](http://a248.e.akamai.net/f/248/3214/1d/www.zones.com/images/static/ss-why-cloud-promo.jpg){ width=80% }
</div>
<div id="right">
![](http://docs.aws.amazon.com/autoscaling/latest/userguide/images/as-basic-diagram.png)
</div>

## Gestión dinámica de la capacidad... ¿otras aplicaciones?

No sólo aplica a servicios expuestos a Internet...

* Virtual Desktop
* Business analytics
* Procesos Industriales

... **la demanda variable es parte de nuestra realidad**.

Y las **expectativas de disponibilidad y optimización de costes** sólo van en aumento

## ¿Como se paga en un Cloud Público?

> Pay-as-you-go

* Bajo demanda
* Subasta
* Reserva
* Por niveles

## Ejercicio sobre pricing

[https://calculator.s3.amazonaws.com/index.html](https://calculator.s3.amazonaws.com/index.html)
