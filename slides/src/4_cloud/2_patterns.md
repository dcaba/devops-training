#

## Patrones para definir la infraestructura

## Stack, la unidad básica

> **Stack**: conjunto de elementos de infraestructura que se definen/empaquetan/gestionan como una unidad

Un stack puede ser un único servidor, toda la infraestructura relacionada con una aplicación o hasta un CPD entero

## Organizando stacks

* Monolítico
* Tiers
* Stacks compartidos
* Microservicios

## Monolítico

![](https://docs.google.com/drawings/d/12OC-_Z7HzOSQ8w6xaVjEQkRDgYdb2H6dK4G35L0r624/pub?w=742&h=934){ width=50% }

## Dividir un entorno de aplicación en múltiples stacks / tiers

![](https://docs.google.com/drawings/d/1SZbMk-rkibmzWa0wrhQUDHtfhf2-2vX9pg2SVKb7yjs/pub?w=889&h=553){ width=50% }

## Compartir stacks entre aplicaciones

![](https://docs.google.com/drawings/d/1rS92zQzceYIWsLoR-FSYa9Qucv05MHS-kd1pyDOfxsM/pub?w=686&h=741){ width=50% }

## Microservicios

![](https://docs.google.com/drawings/d/16q-IsZ2Sr_rm56fDHpkuq51TRT_TeXVekb3mgUzCzXU/pub?w=675&h=705){ width=50% }

## Entornos

* **Despliegues/"instancias" de un mismo stack(s) y servicio**, para propósitos como el testing
    - Por ejemplo: Desarrollo, Pre-producción y Producción
    - Misma infraestructura con diferente dimensionamiento o particularidades de configuración
    - También para sandboxing o "plataformas dedicadas" a propósitos/clientes
    
* **Mantener múltiples entornos de forma consistente** siempre ha sido un reto...
    - ...pero definir la infraestructura como código nos lo facilita

> **NADA** debe quedar fuera de la definición como código!

## Reusando ficheros de definición

* Para mantener la consistencia, debemos tener un **único fichero de definición de stack**
* Cada entorno se **personaliza con parámetros de entorno**
* Ejemplo:

```terraform
variable "environment" {}
resource "aws_instance" "web_server_1" {
    instance_type = "t2.micro"
    tags {
        Enviroment = "${var.environment}"
}
```

## Ventajas de la parametrización

* **Verificar** completamente la infraestructura antes de producción
* Facilita el **self-service**
* **Evita la duplicidad** de código
* **Mejora la adaptabilidad** del stack ante futuros cambios
