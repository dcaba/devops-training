#

## ¿Por qué debemos usar código para gestionar la infraestructura?

* Permite **reusar** la infraestructura en múltiples entornos, garantizando **uniformidad y fiabilidad**
* Mejora el **mantenimiento, auditoría, la colaboración y reduce los tiempos de propagación** 
de cambios mediante la integración con CI/CD
* Facilita la **extensión e integración** con otras partes
* ¡Permite **testear**!
* Desbloquea la **gestión dinámica** de capacidad y la **resolución automática** de problemas


## Como definir infraestructura como código

Esta definición se especifica en ficheros de configuración y 
diferentes herramientas se encargan de provisionar, modificar o eliminar 
los elementos en base a esa definición


## Ejemplo: Stack básico

![](https://docs.google.com/drawings/d/1KT6PNzG5N0R8fcMeozLTfoOlKag3H99PZSQqKln2kcg/pub?w=345&h=271)

## Descriptores HashiCorp Terraform

```terraform
resource "aws_instance" "web_server_1" {
    ami = "ami-5244a300"
    instance_type = "t2.micro"
}
resource "aws_instance" "web_server_2" {
    ami = "ami-5244a300"
    instance_type = "t2.micro"
}
resource "aws_elb" "web_load_balancer" {
    listener {
        instance_port = 80
        instance_protocol = "http"
        lb_port = 80
        lb_protocol = "http"
    }
    instance = [
        "${aws_instance.web_server_1.id}",
        "${aws_instance.web_server_2.id}"
    ]
```

## Descriptores: AWS Cloudformation

```json
{
    "Ec2Instance1" : {
      "Type" : "AWS::EC2::Instance", 
      "Properties" : {
        "ImageId" : "ami-5244a300",
        "KeyName" : { "Ref" : "KeyName" },
        "InstanceType" : "t2.micro",
        "SecurityGroups" : [{ "Ref" : "Ec2SecurityGroup" }],
      }
    },
    "MyLoadBalancer" : {
        "Type" : "AWS::ElasticLoadBalancing::LoadBalancer",
        "Properties" : {
            "AvailabilityZones" : [ "us-east-1a" ],
            "Instances" : [
                { "Ref" : "Ec2Instance1" },
                { "Ref" : "Ec2Instance1" }
            ],
            "Listeners" : [ {
                "LoadBalancerPort" : "80",
                "InstancePort" : "80",
                "Protocol" : "HTTP"
            } ],
        }
    }
}
```

## Scripting: bash script + AWS CLI

```bash
#!/bin/bash

aws elb create-load-balancer --load-balancer-name MyLoadBalancer --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=80" --availability-zones us-east-1a

for i in `seq 1 2`; do
EC2_RUN_RESULT=$(ec2-run-instances --instance-type t2.micro --group default --region us-east-1 --key $EC2_INSTANCE_KEY --user-data-file instance_installs.sh ami-5244a300)
INSTANCE_NAME=$(echo ${EC2_RUN_RESULT} | sed 's/RESERVATION.*INSTANCE //' | sed 's/ .*//')$i
times=0
echo
while [ 5 -gt $times ] && ! ec2-describe-instances $INSTANCE_NAME | grep -q "running"
do
  times=$(( $times + 1 ))
  echo Attempt $times at verifying $INSTANCE_NAME is running...
done
if [ 5 -eq $times ]; then
  echo Instance $INSTANCE_NAME is not running. Exiting...
  exit
fi

aws elb register-instances-with-load-balancer --load-balancer-name MyLoadBalancer --instances $INSTANCE_NAME
done
```

## SDK: AWS SDK para Python (boto3)

```python
import boto3
boto3_session = boto3.Session(region_name=args.region, profile_name=args.profile)
ec2 = boto3_session.client('ec2', region_name=args.region)
ec2.create_instances(ImageId='ami-5244a300', MinCount=1, MaxCount=2)
while True:
    instances = ec2.instances.filter(
        Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
    if len(instances)>1:
        break
elb = boto3_session.client('elb', region_name=args.region)
elb.create_load_balancer(
    LoadBalancerName = 'MyLoadBalancer',
    Listeners = [{
            'Protocol': 'http', 'LoadBalancerPort': 80,
            'InstanceProtocol': 'http', 'InstancePort': 80,
        },],
)      
elb.register_instances_with_load_balancer(
    LoadBalancerName = 'MyLoadBalancer',
    Instances = [
        { 'InstanceId': instances[0]['Name'] },
        { 'InstanceId': instances[1]['Name'] }]
)
```

## Combinación de scripting, SDK y descriptores

```python
#!/usr/bin/env python

from aloisius import Stack
import boto3

from templates.ec2 import template as template_ec2
from templates.elb import template as template_elb

stack_name = lambda x: '-'.join([args.app_name, args.region, x])
ec2 = Stack(
    StackName=stack_name('ec2'),
    TargetState='present',
    RegionName=region_name,
    TemplateBody=template_ec2.to_json(),
)
elb = Stack(
    StackName=stack_name('elb'),
    TargetState='present',
    RegionName=region_name,
    TemplateBody=template_elb.to_json(),
    Parameters={
        'Ec2Instances': ec2.outputs['Ec2Id'],
    },
)
aloisius.stacks.wait()
```
