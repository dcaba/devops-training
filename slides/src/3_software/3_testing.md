#

## Testing

![](https://techbeacon.com/sites/default/files/most_interesting_man_test_in_production_meme.jpg){width=40%}

> *"Testear es de cobardes"*

## Práctica ahora habitual

El **desarrollo de tests** asociados a la implementación de un componente/servicio/aplicación **se ha extendido** enormemente estos últimos años, aunque conceptualmente no es una práctica de reciente creación.

Los equipos de QA desaparecen en favor de equipos de desarrollo con QA como parte del ciclo de éste

> *¿Será el mismo futuro el de los Ingenieros de Sistemas?*


## Pirámide de tests

<!-- https://martinfowler.com/bliki/TestPyramid.html -->
<!-- https://codingjourneyman.com/tag/uncle-bob/page/2/ -->

![](https://jfiaffe.files.wordpress.com/2014/09/tests-pyramid.png)

## Tests por "roles"

* CI (pre-merge)
    * Unit
    * Componente
    * Integración?
    * Análisis de código
        * Estilo
        * Búsqueda de leaks
* Aceptación (post-merge)
    * Sistema/API/UI/End2End
    * Performance
    * Stress
* Validación de entornos
* ¿Monitorización continua como una forma de tests?


## ¿Cuándo se ejecutan los tests?

<!-- https://www.oreilly.com/learning/configuring-a-continuous-delivery-pipeline-in-jenkins -->
![](https://d3ansictanv2wj.cloudfront.net/figure1-59d91a0acef0253e27185507564b71b2.png)


## ¿Cuándo se desarrollan los tests?

* **Clásico:** Tras desarrollar la funcionalidad
* **Ahora habitual:** Cuando se completa una clase/función de código
* **TDD/BDD:** Antes de desarrollar la funcionalidad
<!-- https://cucumber.io/ -->

## ¿Cómo se desarrollan los tests?

Test frameworks:

* "Unit"
    * JUnit, PHPUnit, Unittest
* "Behaviour"
    * Cucumber, RSpec, Spock
* "Stress"
    * Apache JMeter, Apache Gatling, Locust
* UI
    * Selenium

## Ejemplo 1

[boto - librería python para dar interfaces para llamar a la API de AWS](https://github.com/boto/boto/tree/develop/tests)

## Ejemplo 2

```ruby
module WeMeet
    describe Activity do
        context "with aliases" do
            before do
                @activity = Activity.new("Network gaming")
                @activity.alias("Netgaming")
                @activity.alias("LAN parties")
            end
            it "has the expected aliases" do
                expect(@activity.aliases).to include "Netgaming"
                expect(@activity.aliases).to include "LAN parties"
            end
            it "supports partial text searches" do
                expect(@activity).to be_similar_to "gaming"
                expect(@activity).to be_similar_to "LAN"
                expect(@activity).to be_similar_to "network"
                expect(@activity).to be_similar_to "netgaming"
            end
            it "supports comparison also in case the supplied string is a superset" do
                expect(@activity).to be_similar_to "netgaming in DCs"
            end 
        end
    end
end
```

## ¿Qué hay después de los tests?

**La cobertura de tests nunca es del 100%**:

* "*Faltaba cobertura de tests*" como raíz de cualquier problema
* El sistema de release ha de facilitar el despliegue rápidamente, también de fixes, por eso...
* ... las estrategias de rollback están desapareciendo
