#

## Desarrollo de Código y DevOps

DevOps impulsa el uso de código para configurar, desplegar, actualizar y solventar problemas que van desde la infraestructura a la Aplicación.

> **Por ende, una gestión eficiente y eficaz del código pasa a ser un skill fundamental de los equipos y roles DevOps**

## Un código no gestionable ó no fiable sólo aporta más problemas al ecosistema

## No todo vale...

* *"Pero si es un script"*
* *"Si no forma parte del código de producción"*

> Aprovecha todas las "best practices" que han generado años de construcción de aplicaciones

## Ejemplo: Código duplicado y hardcoding

* Punto de partida: Aplicación que necesita un bucket de S3 por cliente

```
$ ls required_s3_buckets/
my_super_customer_1.py my_super_customer_2.py my_super_customer_3.py my_super_customer_4.py my_super_customer_5.py
```

```python
$ cat required_s3_buckets/my_super_customer_1.py
from troposphere import Output, Ref, Template
from troposphere.s3 import Bucket, PublicRead

t = Template()

t.add_description(
    "Bucket management for my_super_customer_1")

s3bucket = t.add_resource(Bucket("S3Bucket", AccessControl=PublicRead,))

t.add_output(Output(
    "BucketName",
    Value=Ref(s3bucket),
    Description="Name of S3 bucket for my_super_customer_1"
))

print(t.to_json())
```

## Necesidad

Queremos que los buckets sólo retengan la información durante 30 días:

```python
from troposphere import Output, Ref, Template
from troposphere.s3 import Bucket, PublicRead

t = Template()

t.add_description(
    "Bucket management for my_super_customer_1")

s3bucket = t.add_resource(Bucket(
    "S3Bucket",
    AccessControl=PublicRead,
    LifecycleConfiguration=LifecycleConfiguration(Rules=[
        LifecycleRule(
            Id="S3BucketRule1",
            Status="Enabled",
            ExpirationInDays=30,
            )
        ])
    ))

t.add_output(Output(
    "BucketName",
    Value=Ref(s3bucket),
    Description="Name of S3 bucket for my_super_customer_1"
))

print(t.to_json())
```

## El problema

* ¿Hemos de modificar los 5 ficheros (que podrían ser 30)?
* ¿Y también re-ejecutar / actualizar cada uno de los 30 stacks?

## Mejor aproximación

```python
def create_bucket_for_customer(customername):
    t = Template()

    t.add_description(
        'Bucket management for {}'.format(customername))

    s3bucket = t.add_resource(Bucket(
        "S3Bucket",
        AccessControl=PublicRead,
        LifecycleConfiguration=LifecycleConfiguration(Rules=[
            LifecycleRule(
                Id="S3BucketRule1",
                Status="Enabled",
                ExpirationInDays=30,
                )
            ])
        ))

    t.add_output(Output(
        "BucketName",
        Value=Ref(s3bucket),
        Description='Name of S3 bucket for {}'.format(customername)
    ))

    return t
```

## El "main" queda...

```python
customers = [
    "my_super_customer_1",
    "my_super_customer_2",
    "my_super_customer_3",
    "my_super_customer_4",
    "my_super_customer_5",
    ]

for customer in customers:
    print(create_bucket_for_customer(customer).to_json())

```

## Aún mejor aproximación

```
$ cat customer_list.csv
#customer_name,retention
my_super_customer_1,30
my_super_customer_2,30
my_super_customer_3,50
my_super_customer_4,60
my_super_customer_5,30
```

```python
customers = read_customer_list_from_csv(customer_list_file)

for customer in customers:
    print(create_bucket_for_customer(customer).to_json())

```

## Y aún habría mejores

* Cloudformation acepta el uso de variables
* ¿Podríamos integrar la automatización del alta de clientes en la llamada a API correspondiente?

#

## Preámbulo: El lenguaje

## Scripting vs Programming

> Nota: no confundir con lenguajes interpretados vs compilados!

Scripts (*¿alguien ha dicho bash?*) son prácticos dado el amplio abanico de comandos de los entornos UNIX (ver estándar SUS)

* Quien tiene soltura con el terminal de un sistema, generará e incluirá fácilmente, y rápidamente, secuencias de éstos en scripts
* En Windows, batches han sido la opción histórica, siendo VBScript "la alternativa completa", y PowerShell "la nueva alternativa"


## Scripting (2)

... pero, en general, no ayudan a generar código mantenible:

* Mínimo soporte para el testing y la gestión de errores
* Trabajan por defecto con variables globales
* No suelen estar orientados a objetos
* Puede faltar el concepto de módulos, librerías o paquetes, haciendo difícil la reusabilidad

## Python es tu amigo :)

**Python** es un **lenguaje interpretado** (inicialmente su uso era habitual en "scripts complejos"), pero se ha extendido su uso (muy práctico para prototyping), **es orientado a objetos** e incluso existen servicios web complejos desarrollados puramente en Python.

Existen multitud de **frameworks de test, librerías y SDK** para interactuar de forma sencilla con proveedores y tecnologías.

Aporta un **shell** para probar snippets / hacer más interactivo el desarrollo.

`pip` es el **gestor de paquetes** asociado a Python. Soporte de virtual environments para instalar paquetes sin requerir permisos en el sistema operativo / pudiendo convivir con otras versiones de paquetes requeridos por otros programas

> **Plantéate aprender Python. ¡La adopción es alta incluso entre personal no técnico!**

## Preámbulo 2: ¿IDE o editor?

Aunque puede parecer buena idea usar un editor en terminal para desarrollar código "cerca" del entorno de ejecución, y hay editores con múltiples plugins para ayudar mucho con el proceso, **una IDE entiende el código y la estructura del proyecto**...

## IDE - Ventajas

* **Facilita refactors y la navegación** por el código (y la documentación asociada)
* **Detecta errores** sintácticos y también semánticos
* **Sugiere** mejoras (capaz de recoger métricas de calidad de código o aplicar estilos) y autocompletados
* Aporta **shortcuts**, una **shell** y la posibilidad de pasar los **tests de forma totalmente integrada**
* e incluso permite interactuar con el **sistema de control de versiones** o la aplicación de issue tracking!

Ejemplo: renombrar una clase

> **Dale una oportunidad a las IDE. Puedes ganar velocidad y confianza**
