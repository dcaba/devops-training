#

## Empaquetado de aplicaciones

## Artefactos

Un artefacto es cualquier producto derivado del desarrollo de software para poder ser utilizado a posteriori

¿En qué consiste?

* Paquetes de Sistemas Operativos (deb/rpm/...)
* Librerías (.dll/.so/.a/.jar/...)
* Ejecutables (.exe/.jar/...)
* o cualquier otra cosa...
    * Incluso documentos (HTML/pdf/chm/...)

## Paquetes de Sistema Operativo

* Muchos sistemas operativos trabajan con sistema de paquetes para facilitar la gestión de software
* Habitualmente se utiliza para **distribuir software**, pero también pueden contener
ficheros de configuración, referencias a otro(s) paquete(s) o cualquier dato
* Su estructura permite ciertas **ventajas frente a los ficheros no estructurados** (tar.gz, zip, rar):
    - Permiten un proceso de **instalación atómico**, en la que scripts de pre/post instalación se ejecutarán de forma automática; si aparece un error el proceso queda completamente abortado
    - Habitualmente los gestores de paquetes son capaces de mantener las configuraciones específicas
    - El uso de un modelo de **dependencias** permite asegurar una jerarquía para garantizar una ejecución, sin requerir intervención humana

## Ejemplo de definición de un Paquete

Usando fpm:
```bash
package_name=your-package-name
version=${VERSION:-$(date +%Y%m%d%H%M%S)}
iteration=${ITERATION:-1}
build_directory=$(get_abs_filename "./pkg/binaries")
dependencies=datadog-config,sumologic-config,strongpt-cli,edge-monitor,python-autoeni

rm -f "$build_directory"/*.rpm
mkdir -p "$build_directory"

fpm -s dir \
	-t rpm \
	-n $package_name \
	-d $dependencies \
	-v "$version" \
	--iteration "$iteration" \
	-p "$build_directory" \
	-C "./pkg/files"
```

## Vagrant

Vagrant (de HashiCorp) es una herramienta que permite **crear y gestionar entornos de virtualizados en un único workflow**

Ofrece un entorno configurable, reproducible y portable para replicar el mismo entorno en múltiples plataformas

![](Resim1_calisma_prensibi.png){ width=50% }

## Poviders vs Provisioners

![](http://i0.wp.com/devopsmates.com/wp-content/uploads/2017/04/workflow.png)

## Vagrantfile

```ruby
require 'yaml'
# Read YAML file with VM details (box, CPU, and RAM)
machines = YAML.load_file(File.join(File.dirname(__FILE__), 'machines.yml'))

  # Iterate through entries in YAML file to create VMs
  machines.each do |machine|
    config.vm.define machine['name'] do |srv|
      srv.vm.hostname = machine['name']
      srv.vm.box = machine['box']
      config.vm.provider "virtualbox" do|vb|
        vb.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
      end
      # Provision the VM with Ansible if enabled in machines.yml
      if machine['provision'] != nil
        srv.vm.provision 'ansible' do |ansible|
          ansible.playbook = machine['provision']
        end # srv.vm.provision
      end # if machine['provision']
    end # config.vm.define
  end # machines.each
end # Vagrant.configure
```

## Contenedores

* Los contenedores aprovechan funcionalidad del kernel del sistema operativo
para mantener su propio sistema de ficheros
y aislar/asignar una partición de los recursos del sistema a dicho contenedor
* Un contenedor es la unidad básica en la cual reside una aplicación, y **contiene TODO lo que la aplicación necesita**
* Cada contenedor tiene sus procesos, memoria, dispositivos y red
* Podemos tener contenedores en entorno físico, virtual o en el Cloud

## Diferencias entre contenedores y VMs

| VMs | Contenedores |
| ------ | ------- |
| Necesitan un hipervisor para abstraer los recursos físicos | El aislamiento se realiza mediante utilidades del SO (namespaces, cgroups) |
| Son pesadas y requieren un SO completo | Son ligeros y facilitan la portabilidad |
| Los recursos originales se "particionan" | Algunos recursos son compartidos o con limitaciones para segmentar |
| El aislamiento fue diseñado con la securización en mente | La securización no era un driver inicial |

![](https://camo.githubusercontent.com/dd121c7a443d219cfa4f8e08119b84e3c0fa31b6/68747470733a2f2f646f6e3038363030793367666d2e636c6f756466726f6e742e6e65742f707333622f626c6f672f696d616765732f323031352d30332d636f6e7461696e65722d6d6f6e69746f72696e672f646f636b65725f70315f342e706e67){ width=40% }

## LXC vs Docker

* Los dos dependen en dos funcionalidades clave del kernel de Linux: cgroups y namespaces
* Docker se basaba en LXC inicialmente, ahora implementa su propia librería *libcontainer*
* La principal diferencia es que con LXC el storage no está limitado al contenedor y cada contenedor implementa un SO funcional

![](linux-vs-docker-comparison-architecture-docker-lxc.png){ width=50% }

## Docker vs CoreOS rkt

| Docker | rkt |
| ------ | ------- |
| Daemon central | Sin Daemon |
| Utiliza imágenes Docker | Utiliza un formato común |
| Binario monolítico | Modular e independiente |

![](https://coreos.com/rkt/docs/latest/rkt-vs-docker-process-model.png){ width=60% }

## Dockerfile

Un contenedor en ejecución parte de un template (base). El resultado de la ejecución de un contenedor
(que puede modificar su filesystem) puede pasar a ser una nueva imagen.

Otra forma más común de generar nuevos contenedores es vía **Dockerfile**, donde definimos:

* Imagen base
* Librerías
* Binarios
* Scripts
* Sistema de ficheros

```docker
FROM python:2.7
ADD ./code
WORKDIR /code
LABEL "DEVOPS"="DevOpsDemo"
RUN pip install -upgrade pip
RUN pip install flask
CMD python MyApp.py
```

## Docker Hub

![](http://www.slashroot.in/sites/default/files/styles/article_image_full_node/public/field/image/Dockerfile%20to%20build%20images_0.PNG)

![](https://blog.docker.com/media/2015/09/docker-hub-diagram.png)


## Comandos Docker básicos

```
* Crear un contenedor con mi aplicación:
    - docker build . -t myapp:1.0.1
* Arrancar el contenedor/aplicatión:
    - docker run -d -p 80 myapp:1.0.1
* Validar contenedores en ejecución
    - docker ps -a
* Copiar contenidos a un contenedor
    - docker cp ~/file.tmp 572762d8669d:/root/file.tmp
* Detener un contenedor
    - docker stop/kill 572762d8669d
* Eliminar un contenedor
    - docker rm 572762d8669d
```
