#

## El proceso de deployment

## dev/QA packages

> ¿Son necesarios?

## Pipeline

![](https://1.bp.blogspot.com/-lH7Cx4sQaN8/WKYXFCB_0wI/AAAAAAAADmY/hXGStkpeL3gl-wZ3uPkFfTIqC2dWDbPlACLcB/s1600/spinnaker-at-waze-4.png)

## Fase 1 - Estación del desarrollador

* Tras desarrollar (idealmente antes/durante/después) **se ejecuta la test suite**
    * TDD hace enforcement de esto
    * Contra más facilidades para ejecutar los tests en local, más ágil es el desarrollo
* Ejecutar la test suite puede implicar:
    * Ejecución de tests unitarios
    * Ejecución de tests de componente
* Gestión de dependencias
    * Arrancar localmente "mocks", y/o...
    * última versión estable, y/o ...
    * conexión a entornos de DEV

## ¿Dónde controlamos la calidad de código?

* Code Reviews
* Herramientas para el Code Quality
    * Pueden aparecer como un check más en una "code review"

## Build

En la etapa de build (que normalmente lanzamos tras el merge a master):

* Se ejecuta la test suite en un entorno controlado
* Se compila código, si aplica
* Se empaqueta el código ejecutable
* Se sube a un repositorio de software

## Baking

Siguiendo un patrón de infraestructura inmutable, preparamos "el entregable", que sería una imágen de máquina virtual o container.

Herramienta por defecto: 

* "Packer", de HashiCorp
* toma el paquete o librerías del repositorio de software
* puede dejar el resultado también en un repositorio (registry, en el caso de containers) o en un Cloud Provider específico

Oportunidades que genera el baking:

* Punto de encuentro de varios equipos (Seguridad - Hardening?)

## Tests por entornos

<!-- https://en.wikipedia.org/wiki/Deployment_environment#Staging -->

* Clásico: DEV/QA/Staging/PRO
* Básico actual: DEV/PRE/PRO

> *¿Cómo gestionar accesos y estado de las bases de datos?*

## Nuevas aproximaciones a la gestión clásica de entornos

¿Promoción a producción de un despliegue existente usando un API Gateway? 

* Modelo de despliegue único
* Definición de dominios para redirigir el tráfico acordemente

## Recordatorio

> **Quieres repetir este proceso muchas veces al día :)**

