<!-- iac_ecosystem.png -->
<!-- https://martinfowler.com/bliki/PhoenixServer.html -->
#

## Base / Deploy / Runtime

## Ecosistema para Infraestructura

| Herramienta | Phoenix (Inmutable) | Snowflake (Estado deseado) |
| ------ | ------- | ------- |
| **Deploy** | - | Cambios config (AWS Cloudformation, Terraform) - Ejecución remota (Ansible) |
| **Runtime** | - | Cambios config (Terraform) - Ejecución remota (Ansible) |

## Ecosistema para Sistemas

| Herramienta | Phoenix (Inmutable) | Snowflake (Estado deseado) |
| ------ | ------- | ------- |
| **Base** | Images (ISO, AMI, BOX etc.) - Tools (Packer, Vagrant, VeeWee) | - |
| **Deploy** | Orquestración (Mesos, AWS ASG, Vagrant) - PXE (Cobbler, FAI, Kickstart) | Provisión (Cloud-init, Cfn-init, KickStart) |
| **Runtime** | - | Cambios config (Chef, Puppet, CFEngine) - Ejecución remota (Ansible, Saltstack) |

## Ecosistema para Aplicaciones

| Herramienta | Phoenix (Inmutable) | Snowflake (Estado deseado) |
| ------ | ------- | ------- |
| **Base** | Container Images (Docker) - Tools (Packer, Dockerfile) | - |
| **Deploy** | Orquestración (Mesos, Kubernetes) - Ejecución remota (Ansible, Saltstack) | Ejecución remota (Fabric, Capristrano, Saltstack, Ansible) |
| **Runtime** | - | Cambios config (Chef, Puppet, CFEngine) - Ejecución remota (Ansible, Saltstack) |


## Ejemplo de uso de Infraestructura Inmutable

> Objetivo: queremos desplegar contenedores en un cluster dinámico sobre un servicio de Cloud público

<!-- Cuidado; la separación entre infra, sistemas, y aplicaciones, aunque entiendo que es lógica, va en contra de la filosofía del curso -->

* Infraestructura:
    * AWS Cloudformation para definir y generar la infraestructura en el Cloud
* Sistemas:
    * Un proceso de Pipeline que crea mediante Packer las AMIs del Cluster
    * AWS ASG para adaptarnos a cambios de carga
    * No utilizaremos cambios de configuración pues nos basamos en infraestructura inmutable
* Aplicaciones:
    * La aplicaciones a desplegar vendrán creadas como contenedores Docker por la Pipeline de aplicación
    * Para correr los contenedores tendremos implementados un sistema de orquestración de cluster como Kubernetes
    * Finalmente, cualquier cambio en la aplicación, siguiendo el principio de infraestructura inmutable, genera un nuevo contenedor a desplegar
