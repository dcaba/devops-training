#

## Kubernetes Service
![](https://d33wubrfki0l68.cloudfront.net/cc38b0f3c0fd94e66495e3a4198f2096cdecd3d5/ace10/docs/tutorials/kubernetes-basics/public/images/module_04_services.svg){ width=50% }

##
* Un Servicio en k8s es una abstracción que define un conjunto lógico de Pods y una política para acceder a ellos
* Los Pods son mortales, cuando un nodo worker muere, también mueren sus Pods, con un servicio se añade la función de **ReplicationController**
* Un Servicio se define mediante un YAML o JSON, como todos los objetos de k8s.
    * Mediante el **LabelSelector** se asocia un Service a un conjunto de Pods
* Aunque cada Pod tiene una IP única interna, estas Ips no se exponen externamente sin un Servicio. Hay diferentes formas de exponer un Servicio:
    * **ClusterIP** (default): Exponer el Servicio con una IP interna, sólo accesible desde dentro del Cluster
    * **NodePort**: Exponer el Servicio en el mismo puerto que el Nodo, accesible desde el exterior <NodeIP>:<NodePort>
    * **LoadBalancer**: Crea un LoabBalancer externo en un entorno Cloud y asigna una IP externa
    * **External Name**: Expone el servicio con un nombre (externalName) mediante un CNAME utilizando kube-dns

## Labels y Selectors

![](https://d33wubrfki0l68.cloudfront.net/b964c59cdc1979dd4e1904c25f43745564ef6bee/f3351/docs/tutorials/kubernetes-basics/public/images/module_04_labels.svg){ width=40% }

* Los Servicios en k8s se relacionan con los Pods mediante labels y selectors

## Describiendo un Service

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: yourapp
  labels:
    app: yourapp
spec:
  type: NodePort
  selector:
    app: yourapp
  ports:
  - protocol: TCP
    port: 5000
    name: http
```

## Describiendo un ReplicationController
```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: yourapp
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: yourapp
    spec:
      containers:
      - name: devopstraining
        image: chadell/dev_test:2
        ports:
        - containerPort: 5000
        livenessProbe:
          httpGet:
            path: /hello
            port: 5000
          initialDelaySeconds: 30
          timeoutSeconds: 1
```

## Desplegamos nuestra aplicación como Servicio

```bash
$ kubectl apply -f lab3/kubernetes/app-service.yaml

$ kubectl get svc
NAME           TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)          AGE
kubernetes     ClusterIP   10.0.0.1     <none>        443/TCP          70d
yourapp        NodePort    10.0.0.236   <none>        8010:32546/TCP   1m

$ kubectl get pods
NAME                    READY     STATUS    RESTARTS   AGE
yourapp-147w5           1/1       Running   0          2m

$ curl $(minikube service yourapp --url)/members
Members!

$ kubectl delete -f lab3/kubernetes/app-service.yaml
```

o bien, directamente:

```bash
$ minikube service yourapp
```

## Ejercicios de ampliación

[Convertir ReplicationController a Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

[Kubernetes Addons](https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/#enable-addons)

[Creación de un Ingress Controller](https://medium.com/@Oskarr3/setting-up-ingress-on-minikube-6ae825e98f82)
