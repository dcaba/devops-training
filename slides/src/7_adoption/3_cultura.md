<!-- https://www.slideshare.net/meriwilliams/dev-ops-inthewildeduservsymposiummay2013compressed -->

#

## Introduciendo la cultura DevOps

## Experimentación con "canarios"/"pilotos"

... en busca de la creación de una historia de éxito mediante un proyecto que siga marque un nuevo rumbo

![](canaries.jpeg){ width=80% }

## Empujando al autoservicio

Convierte a tus equipos en proveedores internos... El autoservicio puede hacer más atractivos, competitivos,
confiables y útiles los servicios internos

![](http://2.bp.blogspot.com/-6z4obDXUzrY/Vii-oAn_LlI/AAAAAAAAeoA/a07AW5VSPdE/s1600/Obelix-et-Falbala.jpg){ width=50% }

## Usando gamificación

> *"No todo en la vida es diversión"* pero, un poco, sienta bien

<div id="left">
<br>
<br>

* [https://devopsgames.com/](https://devopsgames.com/)
* Retos internos
* Acertijos internos
* Sistemas de puntos...
</div>
<div id="right">
![](https://i.pinimg.com/originals/96/bb/ad/96bbad187970f16f6489142942865b8c.jpg)
</div>

## La voz del experto externo

![](https://images-na.ssl-images-amazon.com/images/I/81Vyn%2BKvFsL._SL1200_.jpg){ width=80% }

## Conferencias y cursos...

... como herramientas de aprendizaje y motivación. DevOps Barcelona, QCon, Jax, Interop, Velocity, CloudNative...

![](asterix-et-la-transitalique-image-conference-de-presse-01.jpg){ width=70% }

## Usando planes de desarrollo

<div id="right">
Pueden permitir la introducción de:

* Lectura de libros
* Experimentación con prácticas
* Compartición de conocimiento
* Metas concretas y realizables

</div>
<div id="left">
![](http://www.mediatoon-distribution.com/_caches/jimages/2cba24b62ce6e63faefb6ad0241d82c5ba1879bc.jpg)
</div>

## Sesiones internas y demos

![](http://www.planocritico.com/wp-content/uploads/2014/11/asterix-gladiador-im-des-600x400.jpg)

## Acompañar la adopción de la mano de Agile

![](https://www.artistsuk.co.uk/acatalog/Asterix_indian_rope_trick_mini.jpg)


## Retrospectivas y mejora continua

Siempre busca las lecturas positivas, y potencia la autocrítica de la forma más constructiva y divertida posible

![](http://4.bp.blogspot.com/-ZJvoYe6Xql4/UC425LmIOEI/AAAAAAAAADg/-fHgZwvsfv4/s1600/Untitled.png)

## Apoyarse en la Comunidad

Existe una gran comunidad cerca de tí u online dispuesta a ayudar (por ejemplo Meetups)

![](https://vignette.wikia.nocookie.net/asterix/images/b/b4/Asterix_-_Cast.jpg/revision/latest){ width=75% }

## Compartir objetivos

Definir objetivos compartidos para que gente de diversos equipos remen en la misma dirección

![](http://3.bp.blogspot.com/-5YcH4JWu4t4/Uj8KaNZ1-ZI/AAAAAAAAA4Y/Mzug-h9P9QU/s1600/asterix+-+obelix+y+compa%C3%B1ia+-+46b.jpg)
