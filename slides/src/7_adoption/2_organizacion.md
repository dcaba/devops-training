#

## Encaje DevOps en la organización

## Silos everywhere

Los "silos" son probablemente uno de los adversarios para el cambio en una organización, y especialmente
contrarios a los principios Agile y DevOps

> **No crees silos para romper silos**

## ¿Qué entendemos por un silo?

<div id="left">
![](https://vignette.wikia.nocookie.net/asterix/images/a/ad/Getafix_brewing.jpg/revision/latest?cb=20150710202238)
</div>

<div id="right">
<br><br>
*Equipo, individuo(s) o servicios que centralizan el control/uso de una tecnología, capa o proceso, fácilmente
convirtiéndose en un bloqueante o cuello de botella para la productividad del resto de la organización*
</div>

##

**Ejemplos:**

* Equipo de despliegue, en el que cualquier despliegue requiere su intervención
* Equipo de QA, en el que cualquier validación requiere de involucrarlos
* Equipo de "gestión de infraestructura", en el que cualquier cambio o despliegue de infraestructura pasa por ello


## Sin embargo...

... los "silos" pueden ser el punto de partida. Y una vía evolutiva, y no totalmente disruptiva respecto a la estructura
inicial de la organización, puede existir.

![](https://i.pinimg.com/originals/29/c3/36/29c3369df8b34ba75eac61e55d43f73b.jpg)

## Cambiando las reglas de colaboración entre equipos

<div id="left">
* "Abrir tickets", "solicitar recursos", "asignar tareas en un proyecto" o "entregar una documentación" no es la forma
de crear la chispa que convierta la gestión de infraestructura y procesos en software
    * Mas la colaboración y motivación se ven afectadas

* Por lo contrario:
    * crear "herramientas" (pueden ser simples scripts)
    * abrir APIs
    * proporcionar "instalables" o componentes reusables

    ¡Sí lo puede ser!
</div>

<div id="right">
![](https://i.pinimg.com/originals/4d/41/bd/4d41bde6c793b8a03cc96ce54c9c993b.jpg)
</div>

## Mezclando equipos

* Un mecanismo para compartir conocimiento y facilitar la gestión de dependencias puede consistir en **embeber** ingenieros
en los equipos de producto/cliente/funcionalidad que los consuman
* Es una opción tanto temporal, como de más largo recorrido
    * En el primer caso, el objetivo puede ser "empujar" a una de las partes a adoptar ciertas prácticas o ganar autonomía
    * O probar/ayudar a madurar una API viendo el "extremo cliente"
    * En el segundo, podemos estar remando hacia...

![](mix_uderzo_asterix.jpg){ width=80% }


## DevOps como grupo de interés

<div id="left">
![](Asterix_Obelix_And_Dogmatix_-_Boar_433f8e31-11da-4e3d-92f8-fb5c008bf030.jpg)
</div>

<div id="right">
* DevOps no como un equipo vertical, sino como una estructura horizontal
* Esta estructura puede crear una red de compartición de conocimiento, prácticas y búsqueda de soluciones conjuntas
* Pero evita lineas de reporting paralelas, o usar dichas redes para hacer "push" de grandes proyectos.
</div>

## Consultoría... con cuidado

La consultoría en IT suele ser consecuencia de:

* tecnologías que requieren mucha especialización
* y/u organizaciones que acaban albergando equipos 100% de operaciones

Piénsalo bien antes de entrar en la rueda; consejos y puntos de vista externos pueden ser positivos puntualmente,
pero desacoplar "quien" implementa de "quien" opera puede ser un error.

![](http://s2.static-clubeo.com/uploads/vttarverneslabrochateaugay/news/repas-gaulois__nvyiuo.jpg){ width=40% }

## Y si todo lo demás falla...

... crear una organización paralela puede no ser descabellado. Ésta no tiene por qué competir con la original, e incluso
pueden marcarse objetivos distintos... pero con una convergencia en el horizonte.

![](asterix_rembrandt_image3A.jpg)
