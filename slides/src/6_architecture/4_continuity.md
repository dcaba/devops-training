#

## Continuidad de servicio con Infraestructura dinámica

## Continuidad de datos

La persistencia de los datos es un reto particular de la infraestructura dinámica. ¿cómo podemos afrontarlo?

* **Replicar datos**, simplemente asegurar que los datos de una instancia estén replicados en otra antes de destruirla
* **Regenerar datos**, hay datos que pueden ser generados a partir del *System of record*, 
en estos casos no es necesario proteger los datos, sino ser capaz de regenerarlos
* **Delegar datos**, existen servicios de storage as a service (AWS S3, etc.) o simplemente storage externo (SAN, NFS), 
esto permite desacoplar los problemas y focalizar en cada parte
* **Back up a storage persistente**, los anteriores se refieren principalmente a *runtime data*, 
pero hay datos que deben permanecer después de la ejecución. Aunque utilicemos entornos Cloud,
 no podemos esquivar nuestras responsabilidades y tener claro físicamente donde se almacenan nuestros datos.
    * Truco: utilizar un proveedor diferente de infraestructura para ciertos datos es sencillo y proporciona un extra de seguridad

##  Circuit Breakers

> El fallo sucede, preparemos nuestras aplicaciones para fallar

<div id="left">
![](https://martinfowler.com/bliki/images/circuitBreaker/sketch.png){ width=60% }
</div>
<div id="right">
* Protegemos a una aplicación mediante un wrapper

* Este wrapper ser encarga de monitorizar la respuesta de la aplicación

* Cuando la respuesta no es satisfactoria (threshold), el circuit breaker responde directamente con un error

* Si la aplicación se recupera el circuit breaker reacciona y vuelve al estado normal

* **Permite que las aplicaciones sean más robustas al tener un fallback definido**
</div>

## Disaster Recovery

* Trabajando con infraestructura dinámica debemos estar continuamente preparados para tratar con fallos
* Por tanto, **los mecanismos más eficientes para gestionar un DR son los mismos que usaríamos para las operaciones rutinarias**
* Buenas prácticas:
    * Considerar los peores casos: recuperación desde zero? tiempo? datos perdidos? dependencias con proveedores?
    * ¿Tiene sentido el uso de plataformas en standby, pudiendo recrear o disfrutar de configuraciones activo/activo?
    * Todo el código debe estar bajo control de cambios (recordad de redundar el VCS!)

## Netflix Simian Army

<!-- https://medium.com/netflix-techblog/the-netflix-simian-army-16e57fbab116 -->

<div id="left">
* Chaos Monkey
* Chaos Gorilla
* Chaos Kong
* Janitor Monkey
* Doctor Monkey
* Compliance Monkey
* Latency Monkey
* Security Monkey
</div>
<div id="right">
![](http://diannemarsh.com/wp-content/uploads/2013/05/SimianArmy-240x300.png)
</div>
