#

## Monitoring

## Evolución

![](appnomic_fig1-1024x679.png)

## Objetivos

* Descubrir cuando un servicio no está disponible o se encuentra degradado
* Entender porque ha fallado (post-mortem)
* Observar de la infraestructura para anticipar futuros problemas

## ¿Qué debemos monitorizar? ¿Dónde?
<div id="left">
* Servidores
* Servicios
* Hardware
* Software
</div>
<div id="right">
* Proveedores
* Estado
* Carga
* Performance
</div>

> *DEV, PRE, PRO?*

## Best Practices para gestionar IaC

* Configuración basada en texto (reproducible)
* Consolidar métricas (Ejecución y Recursos) y eventos (utilizar tags!)
* Controlar la experiencia del usuario
* Autoregistrar la infraestructura dinámicamente
* Mantener histórico de elementos no presentes
    * Recordemos que hay instancias que pueden durar horas o minutos...
* Integrable con otras herramientas para consolidar información relacionada

<!-- http://events.linuxfoundation.org/sites/events/files/slides/ContainerCon%20-%20Monitoring%20in%20Motion.compressed.pdf -->

## Funciones

![](https://docs.google.com/drawings/d/1QhRvzPGbr44TgP2iz91pqdtA31cnLx6vS-iAeSC6NbU/pub?w=867&h=493)

<!-- https://thenewstack.io/classes-container-monitoring/ -->

## Ejemplos por función

* Recolección: Collectd
* Ingesta: Apache Kafka, Carbon-relay
* Storage: Elasticsearch, Whisper database
* Procesado y alering: Prometheus, Graphite
* Visualización: Kibana, Grafana

## Ejemplos de Soluciones

|  | In-house | SaaS |
| ------- | ------- | ------- |
| Métricas | Nagios, Sensu | DataDog, New Relic |
| Eventos | ELK, Splunk | Sumologic, Loggly|

Más ejemplos en Monitoringsucks: [https://github.com/monitoringsucks/tool-repos](https://github.com/monitoringsucks/tool-repos)

## Profiling

Las métricas y los eventos pueden señalar que subsistema está fallando, pero: ¿en qué linias de código está fallando?

<!-- ![](http://www.brendangregg.com/Perf/linux_observability_tools.png) -->
![](http://www.brendangregg.com/perf_events/perf_events_map.png){ width=80% }

<!-- http://www.brendangregg.com/perf.html -->

## Profiling example

![](https://image.slidesharecdn.com/scale2015linuxperfprofiling-150221125358-conversion-gate02/95/linux-profiling-at-netflix-13-638.jpg?cb=1425304848)

## Distributed Tracing

* En entornos de microservicios debemos ser capaces de entender la interacción entre ellos, y como afecta cada uno a la ejecución global
* Esto se consigue añadiendo a cada petición de entrada un identificador único
* Cuando **la petición va pasando por cada microservicio**, la información relacionada se guarda con el identificador
* El resultado es una visualización paso a paso que permite entender dónde esta el problema
* Ejemplos: Zipkin, AppDynamics, New Relic

## Zipkin

![](http://zipkin.io/public/img/web-screenshot.png)
