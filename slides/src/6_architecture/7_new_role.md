#


## DevOps vs SRE (Site Reliability Engineer)

1. DevOps no es un rol, es un aspecto cultural de equipo, no personal. En cambio, SRE consiste en la práctica de crear y mantener servicios de alta disponibilidad.
2. Los SREs, a veces, aplica DevOps. "Ingeniero DevOps" muchas veces sólo es una excusa para contratar sysadmins.

> *SRE es en DevOps lo que Scrum es en Agile* , Shaun Norris


## Caracterísiticas de un SRE

* Su foco es garantizar un nivel de fiabilidad y escalabilidad a las aplicaciones
* El rol ideal seria un software engineer con experiencia como sysadmin
* Deben balancear su trabajo entre responder a emergencias y construir sistemas que minimizan los problemas
* A diferencia de Operaciones, los SREs pueden modificar el código de las aplicaciones
* También es su responsabilidad validar que una aplicación tiene la "calidad" suficiente para pasar a producción

##

![](sre_devops.png)

*CC BY-SA devopstopologies.com*
