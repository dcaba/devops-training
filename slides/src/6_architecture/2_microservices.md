#

## Microservicios

## ¿Cómo sacamos partido del Cloud?

El patrón de diseño emerge como respuesta a las posibilidades que el Cloud ofrece.

![](http://icloudseven.com/wp-content/uploads/2016/02/ventajas-de-la-nube-i-cloud-seven-blog.jpg)

> Fomenta y es parte de la definición de las "**Cloud Native Apps**"

## Problemas con Monolitos

<!-- https://www.slideshare.net/oklengineering/microservices-at-okl -->
* Complejidad
    - ¿añadir una nueva funcionalidad es un rompecabezas?
* Acoplamiento
    - ¿tocar un componente afecta totalmente al resto?
* Testing
    - ¿necesitas mucho tiempo para pasar los tests?
* Escalabilidad
    - ¿que pasa si sólo un componente requiere más recursos?
    - ¿qué pasa si varios equipos deben mantener la aplicación?

![](http://3e.org/feeds/ch/90/ch900101.gif)

## Monolitos vs Microservicios

![](https://martinfowler.com/articles/microservices/images/sketch.png){ width=80% }

<!-- https://martinfowler.com/articles/microservices.html -->

> "Microservices are small autonomous services that work together", Sam Newman

## Características de una arquitectura de Microservicios

* Cada servicio es **independiente** y se organiza alrededor de una función de negocio (equipos cross-functional)
* Se comunican a través de **endpoints sencillos** (como protocolos REST) o mediante buses asíncronos (e.g. RabbitMQ)
* **Mínima centralización**: cada uno tiene su propia tecnología, no depende del resto ni de componentes centrales
* Cada servicio gestiona sus **propia estructura de datos** (Polyglot persistence),
debe ser **tolerante a los fallos** de otros servicios y desplegado independientemente

> ¿Os suena **"you build it, you run it!"**?
Añadid **"keep it small, keep it simple"**

## Microservicios vs SOA

> are Microservices SOA done right?

![](http://usblogs.pwc.com/emerging-technology/wp-content/uploads/2017/02/feature02-figure02.jpg)

## API Gateway

<div id="right">
Funcionalidades:

* Routing
* Autenticación
* Terminación SSL
* Control de tráfico
* Métricas: monitorización y reporting
* Integración con Service Discovery
* Load balancing
* Lógica de retry

</div>

<div id="left">

![](https://cdn.wp.nginx.com/wp-content/uploads/2016/04/Richardson-microservices-part2-3_api-gateway.png)

</div>

<!-- https://www.nginx.com/blog/building-microservices-using-an-api-gateway/ -->

## Client vs Server side Service Discovery

<!-- https://www.nginx.com/blog/service-discovery-in-a-microservices-architecture/
https://www.slideshare.net/SreenivasMakam/service-discovery-using-etcd-consul-and-kubernetes -->

<div id="left">
Server-side
![](https://cdn-1.wp.nginx.com/wp-content/uploads/2016/04/Richardson-microservices-part4-3_server-side-pattern.png)
</div>

<div id="right">
Client-side
![](https://cdn-1.wp.nginx.com/wp-content/uploads/2016/04/Richardson-microservices-part4-2_client-side-pattern.png)
</div>

## Service Discovery

> "Distributed systems are hard"

Opciones:

* Anuncios Multicast/Broadcast
* Hardcoded IPs (individuales o rangos) + DNS lookup
* Registrarse individualmente en los servicios clientes
* Service Registry
    - Los servicios se registran via REST API
    - Los consumidores conusltan via REST API o DNS
    - Ofrece capacidad de Health Check y Balanceo de Carga
    - e.g. Consul, Eureka, ZooKeeper, etcd.

<!-- https://www.nginx.com/blog/service-discovery-in-a-microservices-architecture/ -->

## Service Discovery (ejemplo)

```json
# Registro HTTP API

{
    "name": "servicio-frontend",
    "address":  "10.0.0.12",
    "port": 8080,
    "check": {
        "http": "http://10.0.0.12:8080/health",
        "interval": "5s"
    }
}

$ curl -X PUT -d @servicio-frontend.json localhost:8500/v1/agent/service/register

# Consulta HTTP API

$ curl -s http://192.168.99.106:8500/v1/catalog/service/servicio-frontend

[{
    "Node": "f2266cec556",
    "ServiceID": "servicio-frontend",
    "ServiceName": "servicio-frontend",
    "Address":  "10.0.0.12"
    "ServicePort": 8080,
}]
```

## Service Mesh

![](https://res.infoq.com/news/2017/03/linkerd-celebrates-one-year/en/resources/linkerd-diagram.png)

## Microservicios: ¿seguro?

Prerequisitos:

<!-- Diferencia entre provisión y despliegue? -->
* Provisionamiento rápido
* Monitorización básica
* Despliegue rápido

Otras cosas a tener en cuenta:

* Incrementa el overhead operativo
* El ejecución global puede ralentizarse
* Los despliegues locales son más complicados
* La escalabilidad puede complicarse
* Requiere mayor tooling, también para troubleshooting

<!-- https://aadrake.com/posts/2017-05-20-enough-with-the-microservices.html -->
