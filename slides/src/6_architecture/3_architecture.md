#

## Arquitectura de aplicaciones sobre Internet

## Retos de un despliegue global

**Activo-Activo**

* Los servicios deben ser **stateless**
* Los recursos deben ser consumidos localmente (en cada región)
* No debe haber dependencias entre regiones

**¿Cómo lo conseguimos?**

Tooling, componentes y servicios para:

* La (re)distribución de tráfico
* Automatizar despliegues en múltiples regiones / plataformas
* Réplica de Datos asíncrona o sharding

## Ejemplo de Arquitectura

![](https://docs.google.com/drawings/d/1j5ZTsXVS01dq5QRPt_xFDWT4AMbt9tfoMdWOIImY1E0/pub?w=526&h=624)

## CDN Content Delivery Networks

<div id="left">
Beneficios:

* Reducción de tiempo de carga
    * ¿Latencia?
* Adaptación a carga dinámica
    * ¿Escalable?
* Protege el origen de picos de carga
    * ¿Seguridad?
* Minimiza la infraestructura en origen
    * ¿Caching?
* Puede incrementar la disponibilidad
    * Downtimes en origen pueden no afectar al usuario
</div>

<div id="right">

![](cdn.png)

</div>

## Réplica de Datos: CAP theorem

![](captheorem.png)

## NoSQL

![](http://www.kdnuggets.com/wp-content/uploads/sql-nosql-dbs.jpg)

* **Document model**: Los datos se guardan en documentos, tipo estructura JSON (e.g. CouchDB, MongoDB)
* **Graph model**: Las estructura de graphs simplifican el modelado de relaciones entre entidades (e.g. HyperGraphDB, Neo4j)
* **Key-Value & Wide-Column model**: Estructura básica, donde sólo se consulta via Keys (e.g. Cassandra, Redis, DynamoDB)

<!-- https://www.netsolutions.com/insights/5-things-you-must-consider-adopt-nosql-databases-mongodb/ -->
