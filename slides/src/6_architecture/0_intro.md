# Arquitecturas IT

#

## Contenidos

|   |
| --- |
| 12-Factor |
| Arquitectura de aplicaciones: Monolítico vs Microservicios |
| Arquitectura de servicios en Internet |
| Continuidad de servicio |
| Seguridad |
| Monitorización |
| Un nuevo role: SRE |

## Objetivo

Ver los **ingredientes**, en forma de **cambios de diseño y tecnológicos**, que hacen posible 
e influencian a la cultura y forma de trabajar **DevOps**
