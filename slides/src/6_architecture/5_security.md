#


## Seguridad

* Cambios de configuración automáticos
    * Pero también ocultan las trazas de ataques exitosos :(
* Actualizaciones continuas
    * En caso de infraestructura inmutable, puede implicar redespliegues periódicos
* Validar software de terceros
    * Revisión de código (por fabricante, público, directo, outsourced)
    * Penetration test
    * Legal
* Hardening automático
    * Políticas de seguridad
    * Partir de configuraciones/imágenes mínimas
    * Auditar las cuentas de usuarios, los parámetros de sistema y el software instalado contra sus vulnerabilidades conocidas
* La Pipeline automática puede verificar y aplicar políticas de seguridad
    * Pero a su vez puede ser foco de ataques... 
* Segregar cuentas de infraestructura, limita el problema
