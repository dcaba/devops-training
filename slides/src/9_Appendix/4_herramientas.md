#

## Herramientas para la gestión de código/aplicaciones

## Build

* Make
* Ant
* Maven
* Gradle
* Bazel
* Pants

## CI

* Jenkins
* Travis
* Team City
* Gitlab CI
* Bitbucket CI
* Circle CI
* Google Cloud Build
* Buildbot
* Concourse CI

## Code Quality

* SonarQube
* Code climate
* Codacy

## Baking

* Docker build
* Packer

## Software repositories
<!-- https://binary-repositories-comparison.github.io/ -->
* JFrog Artifactory
* Nexus
* ProGet
* openSUSE Build Service (OBS)


## Code Pipelines

* Jenkins
* GoCD
* Spinnaker
* AWS CodeDeploy
* Stackstorm
* Drone.io
