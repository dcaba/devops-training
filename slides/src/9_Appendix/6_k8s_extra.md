#

## Conceptos Kubernetes

## Kube-DNS
<div id=left>
Kube DNS es la función senzilla de descubrimiento de servicios en un cluster.

* Vigila la API de k8s (Servicios y Pods) 
* Corre un servidor DNS (SkyDNS) para servir nombres apuntando a esos recursos
* Añande a ese servidor DNS una capa de proxy cache (DNSmasq)
* Sobreescribe todos los Pods (/etc/resolv.conf) para dirigir las peticiones DNS a este servidor
</div>
<div id=right>
![](https://2.bp.blogspot.com/-Jj4r6bGt1f8/WORRugYMobI/AAAAAAAABBE/HXH-wBGqweQcJbyQA3bqnUtYeN5aOtE9ACEw/s400/dns2.png)
</div>
<!-- https://blog.sophaskins.net/blog/misadventures-with-kube-dns/ 
http://blog.kubernetes.io/2017/04/configuring-private-dns-zones-upstream-nameservers-kubernetes.html
-->

## Kube-Proxy

<div id=left>
![](http://prog3.com/sbdm/img.ptcms/article/201506/11/5579419f29d51_middle.jpg?_=8485)
</div>
<div id=right>
* El proxy de red de k8s corre en cada nodo
* Convierte los Servicios a reglas IPTABLES hacia los pods/nodos
</div>

##

```bash
-A KUBE-SVC-LI77LBOOMGYET5US -m comment --comment "default/showreadiness:showreadiness" -m statistic --mode random --probability 0.20000000019 -j KUBE-SEP-E4QKA7SLJRFZZ2DD[b][c]  
-A KUBE-SVC-LI77LBOOMGYET5US -m comment --comment "default/showreadiness:showreadiness" -m statistic --mode random --probability 0.25000000000 -j KUBE-SEP-LZ7EGMG4DRXMY26H  
-A KUBE-SVC-LI77LBOOMGYET5US -m comment --comment "default/showreadiness:showreadiness" -m statistic --mode random --probability 0.33332999982 -j KUBE-SEP-RKIFTWKKG3OHTTMI  
-A KUBE-SVC-LI77LBOOMGYET5US -m comment --comment "default/showreadiness:showreadiness" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-CGDKBCNM24SZWCMS 
-A KUBE-SVC-LI77LBOOMGYET5US -m comment --comment "default/showreadiness:showreadiness" -j KUBE-SEP-RI4SRNQQXWSTGE2Y 
```


## Networking
En Kubernetes hay 4 problemas básicos a solucionar en temas de conectividad

1. Container-to-container: se soluciona dentro de los pods via localhost
2. Pod-to-Pod : ??
3. Pod-to-Service: se utilizan los Services
4. External-to-Service: se utilizan los Services

## Networking en Docker vs Kubernetes
<div id=left>
Modelo Docker

* Se crea un bridge virtual con direccionamiento RF1918
* Para cada container se crea una veth (en un Namespace espacífico) lo enlaza con ese Bridge
* Lo mismo sucede para otros host, con lo que hay solapamiento de IPs
* Por tanto, un container sólo puede hablar directamente con los containers del mismo host

</div>
<div id=right>
Modelo Kubernetes

* Todos los contenedores deben comunicarse entre si sin NAT
* Todos los nodos deben comunicarse con los contenedores sin NAT
* La IP con la que un contenedor se ve es la misma con la que los otros lo ven
* Docker a solas no es suficiente, existen diversas [soluciones](https://kubernetes.io/docs/concepts/cluster-administration/networking/) para solucionarlo


</div>

 <!-- https://jvns.ca/blog/2017/10/10/operating-a-kubernetes-network/ -->

## Ingress

El Ingress es una colección de reglas para permitir a las conexiones de entrada llegar a los servicios del cluster

```
    internet
        |
   [ Ingress ]
   --|-----|--
   [ Services ]
```

Se puede configurar con URLs públicas, Load Balancing, terminación SSL y otros. Un Ingress es otro recurso más en k8s.

##

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: test
spec:
  rules:
  - host: foo.bar.com
    http:
      paths:
      - backend:
          serviceName: s1
          servicePort: 80
  - host: bar.foo.com
    http:
      paths:
      - backend:
          serviceName: s2
          servicePort: 80
```

##  TLS

```yaml
apiVersion: v1
data:
  tls.crt: base64 encoded cert
  tls.key: base64 encoded key
kind: Secret
metadata:
  name: testsecret
  namespace: default
type: Opaque
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: no-rules-map
spec:
  tls:
  - secretName: testsecret
  backend:
    serviceName: s1
    servicePort: 80
```
