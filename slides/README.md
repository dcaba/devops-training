# Where are the slides?

The deliverable version of the slides are under the `html/` directory

# Where are the actual contents of the slides?

`src/` directory

# How to regenerate the html version of the slides?

* pandoc is required
* execute bin/build.sh
	* You can pass as an optional parameter the folder to process

OR

* docker is required
* execute bin/build-docker.sh

```
 % ./bin/build.sh
INFO: Building src/
DEBUG: cmdline: pandoc -t html5 --template=reveal.js/reveal-template.html --self-contained --section-divs --slide-level 2 -V theme=solarized --include-in-header=reveal.js/css/text.css -s /home/caba/Code/devops-training/slides/src/*.md -o /home/caba/Code/devops-training/slides/html/index.html
```

# Can I also deliver these htmls in a container / via HTTP?

Yes!! There's a(n extremely simple) docker file for that, with the following helper scripts:

```
% bin/build-slides-server.sh
INFO: Running "docker build -t devops-slides:latest ." under "/home/caba/Code/devops-training/slides"
Sending build context to Docker daemon 41.47 MB
Step 1 : FROM nginx:alpine
 ---> b411e34b4606
Step 2 : COPY html/ /usr/share/nginx/html
 ---> Using cache
 ---> c1d0249a50fd
Successfully built c1d0249a50fd
% bin/run-slides-server.sh
INFO: Running "docker run -d -p 80:80 --name devops-slides-srv devops-slides:latest" under "/home/caba/Code/devops-training/slides"
540bf0bbc49e2a1941ffeefbf924800e7c800a5a5f0fd346f1197fbfcd59b307
Created new window in existing browser session.
% bin/stop-slides-server.sh
devops-slides-srv
devops-slides-srv
```

# And what if I want to run this container into the cloud?

Easy! You can use gcloud app flex engine... for that, you will obviously need a google cloud account with enough permissions in a project, and the compute API enabled [here](https://console.developers.google.com/apis)

```
% docker run -ti --name gcloud-config google/cloud-sdk gcloud auth login
% docker run --rm -ti --volumes-from gcloud-config google/cloud-sdk gcloud init
% docker run --rm -ti --volumes-from gcloud-config -v $PWD/.:/data -w /data google/cloud-sdk gcloud app deploy
You are creating an app for project [devops-training-bcn].
WARNING: Creating an App Engine application for a project is irreversible and the region
cannot be changed. More information about regions is at
<https://cloud.google.com/appengine/docs/locations>.

Please choose the region where you want your App Engine application
located:

 [1] asia-east2    (supports standard and flexible)
 [2] asia-northeast1 (supports standard and flexible)
...
 [13] us-west2      (supports standard and flexible)
 [14] cancel
Please enter your numeric choice:  5

Creating App Engine application in project [devops-training-bcn] and region [europe-west]....done.
Services to deploy:

descriptor:      [/data/app.yaml]
source:          [/data]
target project:  [devops-training-bcn]
target service:  [default]
target version:  [20190213t012025]
target url:      [https://devops-training-bcn.appspot.com]


Do you want to continue (Y/n)?  Y
...
Updating service [default] (this may take several minutes)...done.
Setting traffic split for service [default]...done.
Deployed service [default] to [https://devops-training-bcn.appspot.com]

You can stream logs from the command line by running:
  $ gcloud app logs tail -s default

To view your application in the web browser run:
  $ gcloud app browse
```

# How does the html generation framework work??

* uses pandoc to convert from markup to html5 slides
* reveal.js is used and included in the repo
* but the default reveal template in pandoc is too simple...
	* [https://gist.github.com/aaronwolen/5017084] included and improved with the comments
