# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version('>= 2.0')
unless Vagrant.has_plugin?('vagrant-vyos')
  system('vagrant plugin install vagrant-vyos') || exit!
  exit system('vagrant', *ARGV)
end

Vagrant.configure(2) do |config|

  ##### DEFINE VM for router #####
  config.vm.define "router" do |device|
    
    device.vm.hostname = "router" 
    device.vm.box = "higebu/vyos"
    device.vm.box_version = "1.1.7"

    device.vm.provider "virtualbox" do |v|
      v.name = "router"
      v.memory = 768
    end
    #   see note here: https://github.com/pradels/vagrant-libvirt#synced-folders
    device.vm.synced_folder ".", "/vagrant", disabled: true

    # NETWORK INTERFACES      
      # link for eth --> oob-mgmt-switch:
      device.vm.network "private_network", ip: "192.168.0.3"

      # link for eth --> server01:swp3
      device.vm.network "private_network", virtualbox__intnet: "net5", auto_config: false

      # link for eth --> server01:swp3    
      device.vm.network "private_network", virtualbox__intnet: "net6", auto_config: false


  end


  ##### DEFINE VM for server01 #####
  config.vm.define "server01" do |device|
    device.vm.hostname = "server01" 
    device.vm.box = "ubuntu/xenial64"
    device.vm.provider "virtualbox" do |v|
      v.name = "server01"
      v.linked_clone = true
      v.memory = 512
    end
    #   see note here: https://github.com/pradels/vagrant-libvirt#synced-folders
    device.vm.synced_folder ".", "/vagrant", disabled: true

    device.vm.network "private_network", ip: "192.168.0.100"
    device.vm.network "private_network", virtualbox__intnet: "net5", ip: "10.10.0.100"


    # Fixes "stdin: is not a tty" and "mesg: ttyname failed : Inappropriate ioctl for device"  messages --> https://github.com/mitchellh/vagrant/issues/1673
    device.vm.provision :shell , inline: "(sudo grep -q 'mesg n' /root/.profile 2>/dev/null && sudo sed -i '/mesg n/d' /root/.profile  2>/dev/null) || true;", privileged: false

    # Shorten Boot Process - Applies to Ubuntu Only - remove \"Wait for Network\"
    device.vm.provision :shell , inline: "sed -i 's/sleep [0-9]*/sleep 1/' /etc/init/failsafe.conf 2>/dev/null || true"

    device.vm.provision "shell", inline: <<-SHELL
      echo "ubuntu:ubuntu" | sudo chpasswd
    SHELL

    # Install Python2 for Ansible compatibility
    device.vm.provision :shell , inline: "sudo apt-get install python -y"
    
    # Copy public key
    device.vm.provision :shell , inline: "echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfICqzIPjTCd5sSepVTQhr7MSocjYqsLegH8bezWduowiaWv7Aop4l6AMLTqa12Z6juJoBYiq+JsTpDiy33HrcFBlfLCElIc2AR/HhZO61LcFqKcc3j0CikL6MA2GOAwWrG0egi8/gLzseYzTfpUoXJaT5QjD7ysTpRbGoL02T4yRXAO78G9qFgM/FB9QyP/KPpzfh5MJr6vtMbEd/DDXOut5u8TlUFb2Xgth1gntQ1dq4griziuZuulT0Nqc0vx3VYLGg/GdIaJPqg+8koaA49U563EHgfEdltc0oIOJiH2Qm+SZkPk77l/2IXHcWwkaT4MLd6FcSUPvkNRY+XJtd vagrant@mgmt' >> /home/vagrant/.ssh/authorized_keys"

  end

  ##### DEFINE VM for server02 #####
  config.vm.define "server02" do |device|
    device.vm.hostname = "server02" 
    device.vm.box = "ubuntu/xenial64"
    device.vm.provider "virtualbox" do |v|
      v.name = "server02"
      v.linked_clone = true
      v.memory = 512
    end
    #   see note here: https://github.com/pradels/vagrant-libvirt#synced-folders
    device.vm.synced_folder ".", "/vagrant", disabled: true

    device.vm.network "private_network", ip: "192.168.0.101"
    device.vm.network "private_network", virtualbox__intnet: "net6", ip: "10.20.0.101"


    # Fixes "stdin: is not a tty" and "mesg: ttyname failed : Inappropriate ioctl for device"  messages --> https://github.com/mitchellh/vagrant/issues/1673
    device.vm.provision :shell , inline: "(sudo grep -q 'mesg n' /root/.profile 2>/dev/null && sudo sed -i '/mesg n/d' /root/.profile  2>/dev/null) || true;", privileged: false

    # Shorten Boot Process - Applies to Ubuntu Only - remove \"Wait for Network\"
    device.vm.provision :shell , inline: "sed -i 's/sleep [0-9]*/sleep 1/' /etc/init/failsafe.conf 2>/dev/null || true"

    device.vm.provision "shell", inline: <<-SHELL
      echo "ubuntu:ubuntu" | sudo chpasswd
    SHELL

    # Install Python2 for Ansible compatibility
    device.vm.provision :shell , inline: "sudo apt-get install python -y"

    # Copy public key
    device.vm.provision :shell , inline: "echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfICqzIPjTCd5sSepVTQhr7MSocjYqsLegH8bezWduowiaWv7Aop4l6AMLTqa12Z6juJoBYiq+JsTpDiy33HrcFBlfLCElIc2AR/HhZO61LcFqKcc3j0CikL6MA2GOAwWrG0egi8/gLzseYzTfpUoXJaT5QjD7ysTpRbGoL02T4yRXAO78G9qFgM/FB9QyP/KPpzfh5MJr6vtMbEd/DDXOut5u8TlUFb2Xgth1gntQ1dq4griziuZuulT0Nqc0vx3VYLGg/GdIaJPqg+8koaA49U563EHgfEdltc0oIOJiH2Qm+SZkPk77l/2IXHcWwkaT4MLd6FcSUPvkNRY+XJtd vagrant@mgmt' >> /home/vagrant/.ssh/authorized_keys"

  end


  ##### DEFINE VM for MGMT #####
  config.vm.define "mgmt" do |device|
    device.vm.hostname = "mgmt" 
    
    device.vm.box = "ubuntu/xenial64"
    device.vm.provider "virtualbox" do |v|
      v.name = "mgmt"
      v.linked_clone = true
      v.memory = 512
    end
    #   see note here: https://github.com/pradels/vagrant-libvirt#synced-folders
    device.vm.synced_folder ".", "/vagrant", disabled: false

    device.vm.network "private_network", ip: "192.168.0.200"

    # Fixes "stdin: is not a tty" and "mesg: ttyname failed : Inappropriate ioctl for device"  messages --> https://github.com/mitchellh/vagrant/issues/1673
    device.vm.provision :shell , inline: "(sudo grep -q 'mesg n' /root/.profile 2>/dev/null && sudo sed -i '/mesg n/d' /root/.profile  2>/dev/null) || true;", privileged: false

    # Shorten Boot Process - Applies to Ubuntu Only - remove \"Wait for Network\"
    device.vm.provision :shell , inline: "sed -i 's/sleep [0-9]*/sleep 1/' /etc/init/failsafe.conf 2>/dev/null || true"

    # Install Ansible
    device.vm.provision :shell , path: "./helper_scripts/install_ansible.sh"

    # Copy hosts file to /etc/hosts
    device.vm.provision :shell , inline: "sudo cp /vagrant/helper_scripts/hosts /etc/hosts"

    # Copy secret key
    device.vm.provision :shell , inline: "sudo cp /vagrant/helper_scripts/id_rsa /home/vagrant/.ssh/id_rsa"

  end

end
